package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Address;
import com.employees.model.Employee;
import com.employees.model.GeneralDetails;
import com.employees.service.AddressService;
import com.employees.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class AddressControllerTest {

    @Mock
    private AddressService addressService;

    @Mock
    private EmployeeService employeeService;

    @InjectMocks
    private AddressController addressController;

    @Test
    public void testFindAll() {
        List<Address> addresses = Arrays.asList(
                new Address(11L, "ben gurion 5", "haifa", "123", "israel", null),
                new Address(22L, "arlozorov 3", "tel aviv", "456", "israel", null)
        );
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, addresses, new ArrayList<>());

        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(addressService.findByEmployee(employee)).thenReturn(employee.getAddresses());
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.empty());

        // case 1: employee exists
        ResponseEntity<Iterable<Address>> result = addressController.findAll(1L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertArrayEquals(addresses.toArray(), ((List<Address>) result.getBody()).toArray());

        // case 2: employee not exists
        try {
            addressController.findAll(2L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 2L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testFindOne() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Address address = new Address(11L, "ben gurion 5", "haifa", "123", "israel", employee);
        Mockito.when(addressService.findById(11L)).thenReturn(Optional.of(address));
        Mockito.when(addressService.findById(8L)).thenReturn(Optional.empty());

        // case 1: address exists under requested employee
        ResponseEntity<Address> result = addressController.findOne(1L, 11L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(address.getId(), result.getBody().getId());

        // case 2: address exists not under requested employee
        try {
            addressController.findOne(2L, 11L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Address", 2L, 11L).getReason(), ex.getReason());
        }

        // case 3: address not exists
        try {
            addressController.findOne(1L, 8L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Address", 1L, 8L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testCreate() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Address address = new Address(11L, "ben gurion 5", "haifa", "123", "israel", null);
        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(addressService.save(address)).thenReturn(address);
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.empty());

        // case 1: employee exists
        ResponseEntity<Address> result = addressController.create(1L, address);
        Assertions.assertEquals(HttpStatus.CREATED, result.getStatusCode());
        Assertions.assertEquals(address.getId(), result.getBody().getId());
        Assertions.assertEquals(employee.getEmpId(), address.getEmployee().getEmpId());

        // case 2: employee not exists
        try {
            addressController.create(2L, address);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 2L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testUpdate() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Address address = new Address(11L, "ben gurion 5", "haifa", "123", "israel", employee);
        Address updatedAddress = new Address(22L, "arlozorov 3", "tel aviv", "456", "israel", null);

        Mockito.when(addressService.findById(11L)).thenReturn(Optional.of(address));
        Mockito.when(addressService.save(updatedAddress)).thenReturn(updatedAddress);
        Mockito.when(addressService.findById(33L)).thenReturn(Optional.empty());

        // case 1: address exists under requested employee
        ResponseEntity<Address> result = addressController.update(1L, 11L, updatedAddress);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(updatedAddress.getStreet(), result.getBody().getStreet());
        Assertions.assertEquals(employee.getEmpId(), result.getBody().getEmployee().getEmpId());

        // case 2: address exists not under requested employee
        try {
            addressController.update(2L, 11L, updatedAddress);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Address", 2L, 11L).getReason(), ex.getReason());
        }

        // case 3: address not exists
        try {
            addressController.update(1L, 33L, updatedAddress);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Address", 1L, 33L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testDelete() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Address address = new Address(11L, "ben gurion 5", "haifa", "123", "israel", employee);
        Mockito.when(addressService.findById(11L)).thenReturn(Optional.of(address));
        Mockito.when(addressService.findById(8L)).thenReturn(Optional.empty());

        // case 1: address exists under requested employee
        ResponseEntity<Void> result = addressController.delete(1L, 11L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());

        // case 2: address exists not under requested employee
        try {
            addressController.delete(2L, 11L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Address", 2L, 11L).getReason(), ex.getReason());
        }

        // case 3: address not exists
        try {
            addressController.delete(1L, 8L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Address", 1L, 8L).getReason(), ex.getReason());
        }
    }
}
