package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Employee;
import com.employees.model.GeneralDetails;
import com.employees.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Optional;

@SpringBootTest
public class EmployeeControllerTest {

    @Mock
    private EmployeeService employeeService;

    @InjectMocks
    private EmployeeController employeeController;


    @Test
    public void testFindAll() {
        Iterable<Employee> employeeList = new ArrayList<>();
        Mockito.when(employeeService.findAll()).thenReturn(employeeList);

        ResponseEntity<Iterable<Employee>> result = employeeController.findAll();

        Assertions.assertEquals(200, result.getStatusCodeValue());
        Assertions.assertFalse(result.getBody().iterator().hasNext());
    }

    @Test
    public void testFindOne() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.empty());

        // case 1: entity exists
        ResponseEntity<Employee> result = employeeController.findOne(1L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(employee.getEmpId(), result.getBody().getEmpId());

        // case 2: entity not exists
        try {
            employeeController.findOne(2L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee",2L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testCreate() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Mockito.when(employeeService.save(employee)).thenReturn(employee);

        ResponseEntity<Employee> result = employeeController.create(employee);

        Assertions.assertEquals(HttpStatus.CREATED, result.getStatusCode());
        Assertions.assertEquals(employee.getEmpId(), result.getBody().getEmpId());
    }

    @Test
    public void testUpdate() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(new Employee()));
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.empty());
        Mockito.when(employeeService.save(employee)).thenReturn(employee);

        // case 1: entity exists
        ResponseEntity<Employee> result = employeeController.update(1L, employee);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(employee.getEmpId(), result.getBody().getEmpId());

        // case 2: entity not exists
        try {
            employeeController.update(2L, employee);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 2L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testDelete() {
        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(new Employee()));
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.empty());

        // case 1: entity exists
        ResponseEntity<Void> result = employeeController.delete(1L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());

        // case 2: entity not exists
        try {
            employeeController.delete(2L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 2L).getReason(), ex.getReason());
        }
    }
}
