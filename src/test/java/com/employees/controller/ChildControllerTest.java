package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Child;
import com.employees.model.Employee;
import com.employees.model.GeneralDetails;
import com.employees.service.ChildService;
import com.employees.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ChildControllerTest {

    @Mock
    private ChildService childService;

    @Mock
    private EmployeeService employeeService;

    @InjectMocks
    private ChildController childController;

    @Test
    public void testFindAll() {
        List<Child> children = Arrays.asList(
                new Child(11L, "david", "harush", true, null),
                new Child(22L, "stav", "bar-yehuda", false, null)
        );
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), children);

        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(childService.findByEmployee(employee)).thenReturn(employee.getChildren());
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.empty());

        // case 1: employee exists
        ResponseEntity<Iterable<Child>> result = childController.findAll(1L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertArrayEquals(children.toArray(), ((List<Child>) result.getBody()).toArray());

        // case 2: employee not exists
        try {
            childController.findAll(2L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 2L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testFindOne() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Child child = new Child(11L, "david", "harush", true, employee);
        Mockito.when(childService.findById(11L)).thenReturn(Optional.of(child));
        Mockito.when(childService.findById(8L)).thenReturn(Optional.empty());

        // case 1: child exists under requested employee
        ResponseEntity<Child> result = childController.findOne(1L, 11L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(child.getId(), result.getBody().getId());

        // case 2: child exists not under requested employee
        try {
            childController.findOne(2L, 11L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Child", 2L, 11L).getReason(), ex.getReason());
        }

        // case 3: child not exists
        try {
            childController.findOne(1L, 8L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Child", 1L, 8L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testCreate() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Child child = new Child(11L, "david", "harush", true, null);
        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(childService.save(child)).thenReturn(child);
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.empty());

        // case 1: employee exists
        ResponseEntity<Child> result = childController.create(1L, child);
        Assertions.assertEquals(HttpStatus.CREATED, result.getStatusCode());
        Assertions.assertEquals(child.getId(), result.getBody().getId());
        Assertions.assertEquals(employee.getEmpId(), child.getEmployee().getEmpId());

        // case 2: employee not exists
        try {
            childController.create(2L, child);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 2L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testUpdate() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Child child = new Child(11L, "david", "harush", true, employee);
        Child updatedChild = new Child(22L, "stav", "bar-yehuda", false, null);

        Mockito.when(childService.findById(11L)).thenReturn(Optional.of(child));
        Mockito.when(childService.save(updatedChild)).thenReturn(updatedChild);
        Mockito.when(childService.findById(33L)).thenReturn(Optional.empty());

        // case 1: child exists under requested employee
        ResponseEntity<Child> result = childController.update(1L, 11L, updatedChild);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(updatedChild.getFirstName(), result.getBody().getFirstName());
        Assertions.assertEquals(employee.getEmpId(), result.getBody().getEmployee().getEmpId());

        // case 2: child exists not under requested employee
        try {
            childController.update(2L, 11L, updatedChild);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Child", 2L, 11L).getReason(), ex.getReason());
        }

        // case 3: child not exists
        try {
            childController.update(1L, 33L, updatedChild);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Child", 1L, 33L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testDelete() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                null, new ArrayList<>(), new ArrayList<>());
        Child child = new Child(11L, "david", "harush", true, employee);
        Mockito.when(childService.findById(11L)).thenReturn(Optional.of(child));
        Mockito.when(childService.findById(8L)).thenReturn(Optional.empty());

        // case 1: child exists under requested employee
        ResponseEntity<Void> result = childController.delete(1L, 11L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());

        // case 2: child exists not under requested employee
        try {
            childController.delete(2L, 11L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Child", 2L, 11L).getReason(), ex.getReason());
        }

        // case 3: child not exists
        try {
            childController.delete(1L, 8L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Child", 1L, 8L).getReason(), ex.getReason());
        }
    }
}
