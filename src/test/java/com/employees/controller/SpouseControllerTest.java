package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Employee;
import com.employees.model.GeneralDetails;
import com.employees.model.Spouse;
import com.employees.service.EmployeeService;
import com.employees.service.SpouseService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Optional;

@SpringBootTest
public class SpouseControllerTest {

    @Mock
    private SpouseService spouseService;

    @Mock
    private EmployeeService employeeService;

    @InjectMocks
    private SpouseController spouseController;

    @Test
    public void testGetSpouse() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                new Spouse(11L, "shani", "levi", "456"), new ArrayList<>(), new ArrayList<>());
        Employee employee2 = new Employee(2L, new GeneralDetails("tal", "hen", "890"),
                null, new ArrayList<>(), new ArrayList<>());
        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.of(employee2));
        Mockito.when(employeeService.findById(3L)).thenReturn(Optional.empty());

        // case 1: employee exists with spouse
        ResponseEntity<Spouse> result = spouseController.getSpouse(1L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(employee.getSpouse().getId(), result.getBody().getId());

        // case 2: employee exists without spouse
        try {
            spouseController.getSpouse(2L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Spouse", 2L).getReason(), ex.getReason());
        }

        // case 3: employee not exists
        try {
            spouseController.getSpouse(3L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 3L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testCreate() {
        Employee employee = new Employee(1L, new GeneralDetails("tal", "hen", "890"),
                null, new ArrayList<>(), new ArrayList<>());
        Spouse spouse = new Spouse(11L, "adva", "dahan", "345");
        Employee employee2 = new Employee(2L, new GeneralDetails("avi", "cohen", "123"),
                new Spouse(22L, "shani", "levi", "456"), new ArrayList<>(), new ArrayList<>());

        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(spouseService.save(spouse)).thenReturn(spouse);
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.of(employee2));
        Mockito.when(employeeService.findById(3L)).thenReturn(Optional.empty());

        // case 1: employee exists without spouse
        ResponseEntity<Spouse> result = spouseController.create(1L, spouse);
        Assertions.assertEquals(HttpStatus.CREATED, result.getStatusCode());
        Assertions.assertEquals(spouse.getId(), result.getBody().getId());
        Assertions.assertEquals(spouse.getId(), employee.getSpouse().getId());

        // case 2: employee exists with spouse
        try {
            spouseController.create(2L, spouse);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.CONFLICT, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.conflict("Spouse", 2L).getReason(), ex.getReason());
        }

        // case 3: employee not exists
        try {
            spouseController.create(3L, spouse);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 3L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testUpdate() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                new Spouse(11L, "shani", "levi", "456"), new ArrayList<>(), new ArrayList<>());
        Spouse spouse = new Spouse(22L, "adva", "dahan", "345");
        Employee employee2 = new Employee(2L, new GeneralDetails("tal", "hen", "890"),
                null, new ArrayList<>(), new ArrayList<>());

        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(spouseService.save(spouse)).thenReturn(spouse);
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.of(employee2));
        Mockito.when(employeeService.findById(3L)).thenReturn(Optional.empty());

        // case 1: employee exists with spouse
        ResponseEntity<Spouse> result = spouseController.update(1L, spouse);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(spouse.getId(), result.getBody().getId());
        Assertions.assertEquals(spouse.getId(), employee.getSpouse().getId());

        // case 2: employee exists without spouse
        try {
            spouseController.update(2L, spouse);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Spouse", 2L).getReason(), ex.getReason());
        }

        // case 3: employee not exists
        try {
            spouseController.update(3L, spouse);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 3L).getReason(), ex.getReason());
        }
    }

    @Test
    public void testDelete() {
        Employee employee = new Employee(1L, new GeneralDetails("avi", "cohen", "123"),
                new Spouse(11L, "shani", "levi", "456"), new ArrayList<>(), new ArrayList<>());
        Employee employee2 = new Employee(2L, new GeneralDetails("tal", "hen", "890"),
                null, new ArrayList<>(), new ArrayList<>());

        Mockito.when(employeeService.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.when(employeeService.findById(2L)).thenReturn(Optional.of(employee2));
        Mockito.when(employeeService.findById(3L)).thenReturn(Optional.empty());

        // case 1: employee exists with spouse
        ResponseEntity<Void> result = spouseController.delete(1L);
        Assertions.assertEquals(HttpStatus.OK, result.getStatusCode());
        Assertions.assertEquals(null, employee.getSpouse());

        // case 2: employee exists without spouse
        try {
            spouseController.delete(2L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Spouse", 2L).getReason(), ex.getReason());
        }

        // case 3: employee not exists
        try {
            spouseController.delete(3L);
            Assertions.fail();
        } catch (ResponseStatusException ex) {
            Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
            Assertions.assertEquals(ErrorBuilder.notFound("Employee", 3L).getReason(), ex.getReason());
        }
    }
}
