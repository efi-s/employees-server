package com.employees.service;

import com.employees.dal.ChildRepository;
import com.employees.model.Child;
import com.employees.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChildServiceImpl implements ChildService {
    @Autowired
    private ChildRepository childRepository;

    public Iterable<Child> findByEmployee(Employee employee) {
        return childRepository.findByEmployee(employee);
    }

    public Optional<Child> findById(Long id) {
        return childRepository.findById(id);
    }

    public Child save(Child child) {
        return childRepository.save(child);
    }

    public void deleteById(Long id) {
        childRepository.deleteById(id);
    }
}
