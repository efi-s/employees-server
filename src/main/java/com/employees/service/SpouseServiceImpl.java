package com.employees.service;

import com.employees.dal.SpouseRepository;
import com.employees.model.Spouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpouseServiceImpl implements SpouseService {
    @Autowired
    SpouseRepository spouseRepository;

    public Spouse save(Spouse spouse) {
        return spouseRepository.save(spouse);
    }

    public void delete(Spouse spouse) {
        spouseRepository.delete(spouse);
    }
}
