package com.employees.service;

import com.employees.model.Employee;

import java.util.Optional;

public interface EmployeeService {
    Iterable<Employee> findAll();
    Optional<Employee> findById(Long id);
    Employee save(Employee employee);
    void deleteById(Long id);
}
