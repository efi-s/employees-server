package com.employees.service;

import com.employees.model.Spouse;

public interface SpouseService {
    Spouse save(Spouse spouse);
    void delete(Spouse spouse);
}
