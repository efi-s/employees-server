package com.employees.service;

import com.employees.model.Address;
import com.employees.model.Employee;

import java.util.Optional;

public interface AddressService {
    Iterable<Address> findByEmployee(Employee employee);
    Optional<Address> findById(Long id);
    Address save(Address address);
    void deleteById(Long id);
}
