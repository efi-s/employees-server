package com.employees.service;

import com.employees.dal.AddressRepository;
import com.employees.dal.ChildRepository;
import com.employees.dal.EmployeeRepository;
import com.employees.dal.SpouseRepository;
import com.employees.model.Employee;
import com.employees.model.Spouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private ChildRepository childRepository;
    @Autowired
    private SpouseRepository spouseRepository;

    public Iterable<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Optional<Employee> findById(Long id) {
        return employeeRepository.findById(id);
    }

    public Employee save(Employee employee){
        return employeeRepository.save(employee);
    }

    public void deleteById(Long id) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if(optionalEmployee.isPresent()) {
            Employee employee = optionalEmployee.get();
            Spouse spouse = employee.getSpouse();
            if(null != spouse) {
                employee.setSpouse(null);
                spouseRepository.delete(spouse);
            }
            addressRepository.deleteAll(addressRepository.findByEmployee(employee));
            childRepository.deleteAll(childRepository.findByEmployee(employee));
            employeeRepository.deleteById(id);
        }
    }
}
