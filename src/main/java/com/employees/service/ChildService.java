package com.employees.service;

import com.employees.model.Child;
import com.employees.model.Employee;

import java.util.Optional;

public interface ChildService {
    Iterable<Child> findByEmployee(Employee employee);
    Optional<Child> findById(Long id);
    Child save(Child child);
    void deleteById(Long id);
}
