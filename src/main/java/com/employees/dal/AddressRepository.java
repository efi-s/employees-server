package com.employees.dal;

import com.employees.model.Address;
import com.employees.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
    Iterable<Address> findByEmployee(Employee employee);
}
