package com.employees.dal;

import com.employees.model.Child;
import com.employees.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface ChildRepository extends CrudRepository<Child, Long> {
    Iterable<Child> findByEmployee(Employee employee);
}
