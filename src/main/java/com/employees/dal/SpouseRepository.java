package com.employees.dal;

import com.employees.model.Spouse;
import org.springframework.data.repository.CrudRepository;

public interface SpouseRepository extends CrudRepository<Spouse, Long> {
}
