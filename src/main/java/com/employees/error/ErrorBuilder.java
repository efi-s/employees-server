package com.employees.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ErrorBuilder {
    public static ResponseStatusException notFound(String entity, Long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, entity +
                " was not found for parameter {id=" + id + "}");
    }

    public static ResponseStatusException notFound(String entity, Long employee_id, Long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND, entity +
                " was not found for parameters {employee_id=" + employee_id + ",id=" + id + "}");
    }

    public static ResponseStatusException conflict(String entity, Long id) {
        return new ResponseStatusException(HttpStatus.CONFLICT, entity +
                " already exists for parameter {id=" + id + "}");
    }
}
