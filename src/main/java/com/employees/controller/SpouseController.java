package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Employee;
import com.employees.model.Spouse;
import com.employees.service.EmployeeService;
import com.employees.service.SpouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.plaf.SpinnerUI;
import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/employees/{employee_id}/spouse")
public class SpouseController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private SpouseService spouseService;

    @GetMapping
    public ResponseEntity<Spouse> getSpouse(@PathVariable("employee_id") Long employeeId) {
        Optional<Employee> employee = employeeService.findById(employeeId);
        if(!employee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        Spouse spouse = employee.get().getSpouse();
        if(null == spouse) {
            throw ErrorBuilder.notFound("Spouse", employeeId);
        }
        return ResponseEntity.ok(spouse);
    }

    @PostMapping
    public ResponseEntity<Spouse> create(@PathVariable("employee_id") Long employeeId,
                                         @Valid @RequestBody Spouse spouse) {
        Optional<Employee> optionalEmployee = employeeService.findById(employeeId);
        if(!optionalEmployee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        Employee employee = optionalEmployee.get();
        if(null != employee.getSpouse()) {
            throw ErrorBuilder.conflict("Spouse", employeeId);
        }
        spouse = spouseService.save(spouse);
        employee.setSpouse(spouse);
        employeeService.save(employee);
        return ResponseEntity.status(HttpStatus.CREATED).body(spouse);
    }

    @PutMapping
    public ResponseEntity<Spouse> update(@PathVariable("employee_id") Long employeeId,
                                         @Valid @RequestBody Spouse spouse) {
        Optional<Employee> optionalEmployee = employeeService.findById(employeeId);
        if(!optionalEmployee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        Employee employee = optionalEmployee.get();
        if(null == employee.getSpouse()) {
            throw ErrorBuilder.notFound("Spouse", employeeId);
        }
        spouse.setId(employee.getSpouse().getId());
        spouse = spouseService.save(spouse);
        employee.setSpouse(spouse);
        employeeService.save(employee);
        return ResponseEntity.ok(spouse);
    }

    @DeleteMapping
    public ResponseEntity<Void> delete(@PathVariable("employee_id") Long employeeId) {
        Optional<Employee> optionalEmployee = employeeService.findById(employeeId);
        if(!optionalEmployee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        Employee employee = optionalEmployee.get();
        Spouse spouse = employee.getSpouse();
        if(null == spouse) {
            throw ErrorBuilder.notFound("Spouse", employeeId);
        }
        employee.setSpouse(null);
        employeeService.save(employee);
        spouseService.delete(spouse);
        return ResponseEntity.ok().build();
    }
}
