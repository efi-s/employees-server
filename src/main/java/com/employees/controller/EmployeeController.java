package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Employee;
import com.employees.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/employees")
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<Iterable<Employee>> findAll() {
        return ResponseEntity.ok(employeeService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> findOne(@PathVariable Long id) {
        Optional<Employee> employee = employeeService.findById(id);
        if(!employee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", id);
        }
        return ResponseEntity.ok(employee.get());
    }

    @PostMapping
    public ResponseEntity<Employee> create(@Valid @RequestBody Employee employee) {
        return ResponseEntity.status(HttpStatus.CREATED).body(employeeService.save(employee));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> update(@PathVariable Long id,
                                           @Valid @RequestBody Employee employee) {
        if(!employeeService.findById(id).isPresent()) {
            throw ErrorBuilder.notFound("Employee", id);
        }
        employee.setEmpId(id);
        return ResponseEntity.ok(employeeService.save(employee));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        if(!employeeService.findById(id).isPresent()) {
            throw ErrorBuilder.notFound("Employee", id);
        }
        employeeService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
