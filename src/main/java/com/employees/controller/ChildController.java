package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Child;
import com.employees.model.Employee;
import com.employees.service.ChildService;
import com.employees.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/api/employees/{employee_id}/children")
public class ChildController {
    @Autowired
    private ChildService childService;
    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<Iterable<Child>> findAll(@PathVariable("employee_id") Long employeeId) {
        Optional<Employee> employee = employeeService.findById(employeeId);
        if(!employee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        return ResponseEntity.ok(childService.findByEmployee(employee.get()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Child> findOne(@PathVariable("employee_id") Long employeeId,
                                         @PathVariable Long id) {
        Optional<Child> child = childService.findById(id);
        if(!child.isPresent() || !employeeId.equals(child.get().getEmployee().getEmpId())) {
            throw ErrorBuilder.notFound("Child", employeeId, id);
        }
        return ResponseEntity.ok(child.get());
    }

    @PostMapping
    public ResponseEntity<Child> create(@PathVariable("employee_id") Long employeeId,
                                        @Valid @RequestBody Child child) {
        Optional<Employee> employee = employeeService.findById(employeeId);
        if(!employee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        child.setEmployee(employee.get());
        return ResponseEntity.status(HttpStatus.CREATED).body(childService.save(child));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Child> update(@PathVariable("employee_id") Long employeeId,
                                        @PathVariable Long id,
                                        @Valid @RequestBody Child child) {
        Optional<Child> optionalChild = childService.findById(id);
        if(!optionalChild.isPresent()) {
            throw ErrorBuilder.notFound("Child", employeeId, id);
        }
        Employee employee = optionalChild.get().getEmployee();
        if(!employeeId.equals(employee.getEmpId())) {
            throw ErrorBuilder.notFound("Child", employeeId, id);
        }
        child.setId(id);
        child.setEmployee(employee);
        return ResponseEntity.ok(childService.save(child));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("employee_id") Long employeeId,
                                       @PathVariable Long id) {
        Optional<Child> child = childService.findById(id);
        if(!child.isPresent() || !employeeId.equals(child.get().getEmployee().getEmpId())) {
            throw ErrorBuilder.notFound("Child", employeeId, id);
        }
        childService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
