package com.employees.controller;

import com.employees.error.ErrorBuilder;
import com.employees.model.Address;
import com.employees.model.Employee;
import com.employees.service.AddressService;
import com.employees.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/api/employees/{employee_id}/addresses")
public class AddressController {
    @Autowired
    private AddressService addressService;
    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<Iterable<Address>> findAll(@PathVariable("employee_id") Long employeeId) {
        Optional<Employee> employee = employeeService.findById(employeeId);
        if(!employee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        return ResponseEntity.ok(addressService.findByEmployee(employee.get()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Address> findOne(@PathVariable("employee_id") Long employeeId,
                                           @PathVariable Long id) {
        Optional<Address> address = addressService.findById(id);
        if(!address.isPresent() || !employeeId.equals(address.get().getEmployee().getEmpId())) {
            throw ErrorBuilder.notFound("Address", employeeId, id);
        }
        return ResponseEntity.ok(address.get());
    }

    @PostMapping
    public ResponseEntity<Address> create(@PathVariable("employee_id") Long employeeId,
                                          @Valid @RequestBody Address address) {
        Optional<Employee> employee = employeeService.findById(employeeId);
        if(!employee.isPresent()) {
            throw ErrorBuilder.notFound("Employee", employeeId);
        }
        address.setEmployee(employee.get());
        return ResponseEntity.status(HttpStatus.CREATED).body(addressService.save(address));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Address> update(@PathVariable("employee_id") Long employeeId,
                                          @PathVariable Long id,
                                          @Valid @RequestBody Address address) {
        Optional<Address> originalAddress = addressService.findById(id);
        if(!originalAddress.isPresent()) {
            throw ErrorBuilder.notFound("Address", employeeId, id);
        }
        Employee employee = originalAddress.get().getEmployee();
        if(!employeeId.equals(employee.getEmpId())){
            throw ErrorBuilder.notFound("Address", employeeId, id);
        }
        address.setId(id);
        address.setEmployee(employee);
        return ResponseEntity.ok(addressService.save(address));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("employee_id") Long employeeId,
                                       @PathVariable Long id) {
        Optional<Address> address = addressService.findById(id);
        if(!address.isPresent() || !employeeId.equals(address.get().getEmployee().getEmpId())) {
            throw ErrorBuilder.notFound("Address", employeeId, id);
        }
        addressService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
