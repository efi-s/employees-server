# Employee Management #

### What is Employee Management for? ###

Employee Management is a management system for employees in a company.  
The project uses Spring Boot, Java and Gradle.  
The application exposes REST APIs to perform CRUD operations on the entities in the system.

Entities: 
* Employee - main entity  
* Spouse  
* Address  
* Child  

### Getting started ###
##### Setup:
* Clone the project with git.
* Open with IntelliJ IDE (Recommended)
* Lombok Plugin - Install the plugin by the following steps in IntelliJ:
    1. Go to File > Settings > Plugins
    2. Search for Lombok
    3. Click on Install plugin
    4. Restart IntelliJ IDEA

##### Usage:
* Server's base url: http://localhost:8080/api
* Swagger - exposes RESTful API Documentation of the implemented endpoints in the application. Available on http://localhost:8080/swagger-ui.html.
* H2 Database - the project uses H2 in-memory database, it embedded in the project and no need installation or configuration of this. H2 console enabled on http://localhost:8080/h2, the DB name is employeedb and the user name and password are the defaults.
* Postman Collection - under postman folder in this repository you will find json file with postman collection, download and import it to postman. It includes all API operations on the system.
* Unit Tests - under src/test path in the repository you will find the unit test classes. Junit5 and Mockito were used in writing that tests.
* Actuator - Spring boot actuator supply information about the application like health check, metrics gathering etc. Available on http://localhost:8080/actuator.  
